var express = require('express');
var router = express.Router();
var models  = require('../models');

import Service from '../service/exampleservice';

/* GET users listing. */
router.get('/', function(req, res, next) {

    var serv;

    serv = new Service('hello');
    serv.printMsg();

    models.Item.findAll({
        limit: 2,
        offset: 0
    }).then(function(result) {
        res.json(result);
    });
});

router.post('/', function(req, res, next) {
    models.Order.findById(req.body.OrderId).then(function (order) {
        if (!order) {
            res.json({error: 'order not found'});
        } else {
            models.Item.create({
                productSKU: req.body.productSKU,
                price: req.body.price,
                amount: req.body.amount,
                OrderId: req.body.OrderId
            }).then(function(u) {
                res.json(u);
            });
        }
    });
});

module.exports = router;
