var express = require('express');
var router = express.Router();
var models  = require('../models');

import Service from '../service/exampleservice';

/* GET users listing. */
router.get('/', function(req, res, next) {

    var serv;

    serv = new Service('hello');
    serv.printMsg();

    models.Order.findAll({
        include: [ models.Item ],
        limit: 50,
        offset: 0
    }).then(function(result) {
        var mapped;

        mapped = result.map(function(o) {
            var n = {};

            n.id = o.id;
            n.orderNr = o.orderNr;
            n.description = o.description;
            n.createdAt = o.createdAt;
            n.updatedAt = o.updatedAt;
            n.price = o.Items.reduce(function(acc, val) {
                return acc + val.price;
            }, 0);
            return n;
        });

        res.json(mapped);
    });
});

router.put('/:orderId', function(req, res, next) {

    models.Order.findById(req.params.orderId).then(function (order) {
        if (order) {
            order.update(req.body).then(function(order) {
                res.json(order);
            });
        } else {
            res.json({
                error: 'order not found'
            });
        }
    });
});


router.get('/:orderId/items', function(req, res, next) {

    models.Order.findById(req.params.orderId).then(function (order) {
        if (order) {
            return order.getItems();
        } else {
            res.json({
                error: 'order not found'
            });
        }
    }).then(function (items) {
        res.json(items);
    });
});

router.post('/', function(req, res, next) {
    models.Order.create({
        orderNr: req.body.orderNr,
        description: req.body.description
    }).then(function(u) {

        console.log('got post', req.path);

        req.app.session.publish('/api/order', [u], {
            eventType: 'add'
        });

        res.json(u);
    });
});

module.exports = router;
