export default class ResourceTypeManager {

    /**
     * register a collection of resource types
     */
    register (app, types) {

        types.forEach(function (t) {

            if (t.get) {
                app.get(t.identifier, function () {
                    t.get.apply(t, arguments);
                });
            }

            if (t.post) {
                app.post(t.identifier, function () {
                    t.post.apply(t, arguments);
                });
            }

            if (t.put) {
                app.put(t.identifier, function () {
                    t.put.apply(t, arguments);
                });
            }

            if (t.delete) {
                app.delete(t.identifier, function () {
                    t.delete.apply(t, arguments);
                });
            }

            console.log('Registered ResourceType with identifier:', t.identifier);
        });
    }
}