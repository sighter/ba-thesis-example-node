
export default class ResourceType {

    constructor(identifier, mapper, requestIdProperty, topicType) {

        if (!identifier) {
            throw 'A ResourceType identifier is required';
        }

        this.identifier = identifier;
        this.mapper = mapper;
        this.requestIdProperty = requestIdProperty;
        this.topicType = topicType;

    }

    get (req, res, next) {
        var id = req.params[this.requestIdProperty];

        console.log('got id: ', id);

        if (!id) {
            throw `No id in request, prop was ${this.requestIdProperty}`;
        }

        this.mapper.findById(id).then((inst) => {
            res.json(inst);
        }, (msg) => {
            res.json({error: 'error'});
        });
    }

    put (req, res, next) {
        var id = req.params[this.requestIdProperty];

        if (!id) {
            throw `No id in request, prop was ${this.requestIdProperty}`;
        }

        return new Promise((resolve, reject) => {
            this.mapper.update(id, req.body).then((inst) => {
                res.json(inst);
                resolve(inst);
            }, (msg) => {
                res.json({error: msg});
                reject(msg);
            });
        });
    }

    delete (req, res, next) {
        throw 'Not implemented!';
    }
}