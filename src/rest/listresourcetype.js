import ResourceType from './resourcetype';

export default class ListResourceType extends ResourceType {

    constructor(identifier, elementType, topicType) {

        if (!elementType) {
            throw 'A ListResourceType must have an elementType';
        }

        super(identifier, undefined, undefined, topicType);

        this.elementType = elementType;
    }

    get (req, res, next) {
        this.elementType.mapper.list().then((result) => {
            res.json(result);
        });
    }

    post (req, res, next) {
        return new Promise((resolve, reject) => {
            this.elementType.mapper.create(req.body).then((result) => {
                res.json(result);
                resolve(result);
            });
        });
    }

    delete (req, res, next) {

    }
}