export default class WampService {

    constructor(app) {
        this.app = app;
    }

    publish (topic, args, kwargs) {
        this.app.session.publish(topic, args, kwargs);
    }
}