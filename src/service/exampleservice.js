export default class Service {

    constructor(msg) {
        this.msg = msg || '';
    }

    printMsg() {
        console.log('Message was ' + this.msg);
    }
}