export default class BaseMapper {

    constructor(msg) {
        this.msg = msg || '';
    }

    findById () {
        throw 'Not Implemented';
    }

    list () {
        throw 'Not Implemented';
    }

    create (obj) {
        throw 'Not Implemented';
    }

    update (obj) {
        throw 'Not Implemented';

    }

    delete (obj) {
        throw 'Not Implemented';
    }
}