import BaseMapper from './basemapper';

export default class SequelizeMapper extends BaseMapper{

    constructor (model, include=[]) {

        if (!model) {
            throw 'Model is required';
        }

        super();

        this.include = include;
        this.model = model;
        this.fields = Object.keys(this.model.rawAttributes);
    }

    mapInstanceOut (inst) {
        var obj = {};
        this.fields.forEach((fname) => {
            obj[fname] = inst[fname];
        });
        return obj;
    }

    mapIntoInstance (obj) {
        var inst = {};
        this.fields.forEach((fname) => {
            if (obj[fname] !== undefined && fname !== 'id') {
                inst[fname] = obj[fname];
            }
        });
        return inst;
    }

    findById (id) {
        return new Promise((resolve, reject) => {
            this.model.findById(id, {
                include: this.include
            }).then((inst) => {
                if (inst) {
                    resolve(this.mapInstanceOut(inst));
                } else {
                    reject('Instance not found');
                }
            });
        });
    }

    list (where={}) {
        return new Promise((resolve, reject) => {
            this.model.findAll({
                include: this.include,
                where: where,
                limit: 50,
                offset: 0
            }).then((result) => {
                resolve(result.map((e) => {
                    return this.mapInstanceOut.apply(this, [e]);
                }));
            });
        });
    }

    create (obj) {
        return new Promise((resolve, reject) => {
            this.model.create(
                this.mapIntoInstance(obj), {
                    include: this.include
                }
            ).then((created) => {
                resolve(this.mapInstanceOut(created));
            });
        });
    }

    update (id, obj) {
        var inst = this.mapIntoInstance(obj);

        return new Promise((resolve, reject) => {
            this.model.findById(id, {
                include: this.include
            }).then((found) => {
                if (found) {
                    found.set(inst);
                    found.save(inst).then((updated) => {
                        resolve(this.mapInstanceOut(updated));
                    });
                } else {
                    reject('instane not found');
                }
            });
        });
    }
}