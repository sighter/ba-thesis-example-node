export default class TopicType {

    constructor(identifier, wampservice) {

        this.wampservice = wampservice;
        this.identifier = identifier;

    }

    // not needed for now
    getTopicOptions () {
        var match = this.identifier.match(/:([A-Za-z0-9]+)/g) || [];

        return match.map((el) => {
            return el.replace(/^:/, '');
        });
    }

    emitAddEvent (obj, options={}) {
        var topic;
        topic = this.identifier;

        Object.keys(options).forEach(function (key) {
            var val = options[key];

            topic = topic.replace(':' + key, val);
        });

        console.log('emitting listAdd event to', topic, obj);

        this.wampservice.publish(topic, [obj], {
            eventType: 'add'
        });
    }

    emitUpdateEvent (obj, options={}) {
        var topic;
        topic = this.identifier;

        Object.keys(options).forEach(function (key) {
            var val = options[key];

            topic = topic.replace(':' + key, val);
        });

        console.log('emitting update event to', topic, obj);

        this.wampservice.publish(topic, [obj], {
            eventType: 'update'
        });
    }
}