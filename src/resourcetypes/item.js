import ResourceType from '../rest/resourcetype';
import ListResourceType from '../rest/listresourcetype';
import SequelizeMapper from '../service/sequelizemapper';
import models from '../models';

export class ItemResourceType extends ResourceType {

    constructor() {
        super(
            '/api/item/:itemId',
            new SequelizeMapper(models.Item),
            'itemId'
        );
    }
}

export class ItemListResourceType extends ListResourceType {

    constructor() {
        super('/api/item/', new ItemResourceType());
    }
}