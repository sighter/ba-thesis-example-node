import SequelizeMapper from '../service/sequelizemapper';

import OrderMapper from '../mapper/order';

import ListResourceType from '../rest/listresourcetype';
import ResourceType from '../rest/resourcetype';
import TopicType from '../ps/topictype';

import models from '../models';

import {ItemResourceType} from '../resourcetypes/item';

export class OrderResourceType extends ResourceType {

    constructor(wampService) {
        super(
            '/api/order/:orderId',
            new OrderMapper(),
            'orderId',
            new TopicType('/api/order', wampService)
        );
        this.model = models.Order;
    }

    put (req, res, next) {
        super.put(req, res, next).then((res) => {
            this.topicType.emitUpdateEvent(res);
        });
    }
}

export class OrderListResourceType extends ListResourceType {

    constructor(wampService) {
        super(
            '/api/order',
            new OrderResourceType(),
            new TopicType('/api/order', wampService)
        );
    }

    post (req, res, next) {
        super.post(req, res, next).then((res) => {
            this.topicType.emitAddEvent(res);
        });
    }

    delete (req, res, next) {

    }
}

/**
 * order items
 */
export class OrderItemListResourceType extends ListResourceType {

    constructor(wampService) {
        super(
            '/api/order/:orderId/items',
            new ItemResourceType(),
            new TopicType('/api/order/:orderId/items', wampService)
        );

        this.orderListTopicType = new TopicType('/api/order', wampService);
        this.orderMapper = new OrderMapper();
    }

    get (req, res, next) {
        var orderId = req.params.orderId;

        this.elementType.mapper.list({
            OrderId: orderId
        }).then((result) => {
            res.json(result);
        });
    }

    post (req, res, next) {
        req.body.OrderId = req.params.orderId;
        super.post(req, res, next).then((res) => {
            this.topicType.emitAddEvent(res, {
                orderId: req.body.OrderId
            });

            this.orderMapper.findById(req.body.OrderId).then((order) => {
                this.orderListTopicType.emitUpdateEvent(order);
            });
        });
    }
}

export class OrderItemResourceType extends ResourceType {

    constructor(wampService) {
        super(
            '/api/order/:orderId/items/:itemId',
            new SequelizeMapper(models.Item),
            'itemId'
        );

        this.orderMapper = new OrderMapper();
        this.orderListTopicType = new TopicType('/api/order', wampService);
        this.orderItemListTopicType = new TopicType('/api/order/:orderId/items', wampService);
    }

    put (req, res, next) {
        super.put(req, res, next).then((res) => {
            this.orderMapper.findById(req.params.orderId).then((order) => {
                this.orderItemListTopicType.emitUpdateEvent(res, {
                    orderId: req.params.orderId
                });
                this.orderListTopicType.emitUpdateEvent(order);
            });
        });
    }
}

export class ChangeOrderCurrencyResourceType extends ResourceType {

    constructor(wampService) {
        super(
            '/api/order/:orderId/changecurrency',
            new OrderMapper(),
            'orderId',
            new TopicType('/api/order', wampService)
        );

        this.itemMapper = new SequelizeMapper(models.Item);
        this.orderListTopicType = new TopicType('/api/order', wampService);
        this.orderItemListTopicType = new TopicType('/api/order/:orderId/items', wampService);
    }

    post (req, res, next) {
        this.mapper.changeCurrency(
            req.params.orderId,
            req.body.currency
        ).then(() => {
            res.json({currency: req.body.currency});

            this.mapper.findById(req.params.orderId).then((order) => {
                this.topicType.emitUpdateEvent(order);
            });

            this.itemMapper.list({
                OrderId: req.params.orderId
            }).then((result) => {
                result.forEach((item) => {
                    this.orderItemListTopicType.emitUpdateEvent(item, {
                        orderId: req.params.orderId
                    });
                });
            });
        });
    }


}