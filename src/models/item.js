'use strict';
module.exports = function(sequelize, DataTypes) {
    var Item;

    Item = sequelize.define('Item', {
        productSKU: DataTypes.STRING,
        price: DataTypes.INTEGER,
        priceCurrency: DataTypes.STRING,
        quantity: DataTypes.INTEGER,
        description: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function(models) {
                Item.belongsTo(models.Order, {
                    onDelete: "CASCADE",
                    foreignKey: {
                        allowNull: false
                    }
                });
            }
        }
    });

    return Item;
};