'use strict';
module.exports = function(sequelize, DataTypes) {
    var Order;

    Order = sequelize.define('Order', {
        orderNr: DataTypes.STRING,
        description: DataTypes.TEXT
    }, {
        classMethods: {
            associate: function(models) {
                Order.hasMany(models.Item);
            }
        }
    });

    return Order;
};