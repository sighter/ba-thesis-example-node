var models = require("../models");

models.sequelize.sync().then(function () {
    return models.Order.bulkCreate([{
        orderNr: 'ON445512',
        description: 'some example order'
    }, {
        orderNr: 'ON128399',
        description: 'some other example order'
    }]);
}).then(function () {
    return models.Order.findAll();
}).then(function (orders) {
    console.log('Added users');

    return models.Item.bulkCreate([{
        productSKU: 'PACH341',
        price: 2000,
        priceCurrency: 'EUR',
        quantity: 3,
        OrderId: 1
    }, {
        productSKU: 'PACH343',
        price: 1236,
        priceCurrency: 'EUR',
        quantity: 10,
        OrderId: 1
    }]);
});