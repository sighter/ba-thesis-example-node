var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
import bodyParser from 'body-parser';

var index = require('./routes/index');
var order = require('./routes/order');
var item = require('./routes/item');

import ResourceTypeManager from './rest/resourcetypemanager';
import WampService from './service/wampservice';

import {
    OrderResourceType,
    OrderListResourceType,
    OrderItemListResourceType,
    OrderItemResourceType,
    ChangeOrderCurrencyResourceType
} from './resourcetypes/order';

import {ItemResourceType, ItemListResourceType} from './resourcetypes/item';

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

if (app.get('env') === 'production') {
  console.error('Using production path: ', 'extapp/build/production/Ord');
  app.use('/app', express.static('extapp/build/production/Ord'));
} else {
  console.error('Using dev path');
  app.use('/app', express.static('extapp'));
}

//app.use('/app', express.static('extapp'));
// app.use('/api/order', order);
// app.use('/api/item', item);

var man = new ResourceTypeManager();
var wampService = new WampService(app);

man.register(app, [
    new OrderResourceType(wampService),
    new OrderListResourceType(wampService),
    new OrderItemListResourceType(wampService),
    new ItemResourceType(wampService),
    new ItemListResourceType(wampService),
    new OrderItemResourceType(wampService),
    new ChangeOrderCurrencyResourceType(wampService)
]);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.error(err);

  // render the error page
  res.status(err.status || 500);
  res.json({error: err});
});

module.exports = app;
