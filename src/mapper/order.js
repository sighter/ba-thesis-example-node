import SequelizeMapper from '../service/sequelizemapper';
import models from '../models';

export default class OrderMapper extends SequelizeMapper {

    constructor () {
        super(
            models.Order,
            [models.Item]
        );
    }

    mapInstanceOut (inst) {
        var obj = super.mapInstanceOut(inst);

        if (inst.Items) {
            obj.price = inst.Items.reduce(function(acc, val) {
                return acc + (val.price * val.quantity);
            }, 0);

            if (inst.Items.length > 0) {
                obj.priceCurrency = inst.Items[0].priceCurrency;
            } else {
                obj.priceCurrency = null;
            }

        } else {
            obj.price = 0;
        }

        return obj;
    }

    changeCurrency (orderId, currency) {
        return models.Item.update({
            priceCurrency: currency
        }, {
            where: {
                OrderId: orderId
            }
        });
    }
}
