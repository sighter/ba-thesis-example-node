/**
 * This view is an example list of people.
 */
Ext.define('Ord.view.main.List', {
    extend: 'Ext.grid.Panel',
    xtype: 'mainlist',

    requires: [
        'Ord.store.Order'
    ],

    title: 'Orders',

    store: {
        type: 'order'
    },

    columns: [
        { text: 'OrderNr',  dataIndex: 'orderNr' },
        {
            text: 'Price',
            dataIndex: 'price',
            renderer: function (value) {
                return (value / 100).toFixed(2);
            }
        },
        { text: 'CCY.', dataIndex: 'priceCurrency', width: 60},
        { text: 'Description', dataIndex: 'description', flex: 1 },
        { text: 'Created', dataIndex: 'createdAt', width: 200}
    ],

    tbar: [{
        xtype: 'button',
        text: 'Create New Order',
        listeners: {
            click: 'onCreateOrderButtonClick'
        }
    }],

    listeners: {
        select: 'onItemSelected'
    }
});
