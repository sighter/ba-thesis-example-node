Ext.define('Ord.view.main.CurrencyComboBox', {

    requires: [
        'Ext.data.Store'
    ],

    extend: 'Ext.form.field.ComboBox',

    xtype: 'currencycombobox',

    constructor: function (config) {
        config = config || {};

        config.store = Ext.create('Ext.data.Store', {
            data: [{
                name: 'EUR'
            }, {
                name: 'GBP'
            }, {
                name: 'USD'
            }]
        });

        config.valueField = 'name';
        config.displayField = 'name';

        this.callParent([config]);
    }
});