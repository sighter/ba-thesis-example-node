/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Ord.view.main.Main', {
    extend: 'Ext.container.Container',
    xtype: 'app-main',

    requires: [
        'Ext.button.Button',
        'Ext.plugin.Viewport',
        'Ext.tab.Panel',
        'Ext.window.MessageBox',

        'Ord.view.main.ItemList',
        'Ord.view.main.List',
        'Ord.view.main.MainController',
        'Ord.view.main.MainModel',
        'Ord.view.main.OrderForm'
    ],

    controller: 'main',
    viewModel: 'main',

    header: {
        layout: {
            align: 'stretchmax'
        },
        title: {
            bind: {
                text: '{name}'
            },
            flex: 0
        },
        iconCls: 'fa-th-list'
    },

    tabBar: {
        flex: 1,
        layout: {
            align: 'stretch',
            overflowHandler: 'none'
        }
    },

    layout: 'border',

    responsiveConfig: {
        tall: {
            headerPosition: 'top'
        },
        wide: {
            headerPosition: 'left'
        }
    },

    items: [{
        region: 'center',
        layout: 'fit',
        xtype: 'mainlist'
    }, {
        region: 'east',
        width: 400,
        xtype: 'tabpanel',
        disabled: true,
        reference: 'tabPanel',
        style: {
            borderWidth: '0 0 0 10px',
            borderColor: '#5fa2dd',
            borderStyle: 'solid'
        },
        items: [{
            title: 'Details',
            xtype: 'orderform',
            mode: 'display',
            reference: 'orderForm'
        }, {
            xtype: 'itemlist',
            reference: 'itemList',
            title: 'Items'
        }],
        bbar: ['->', {
            xtype: 'button',
            text: 'Edit Order',
            listeners: {
                click: 'onOrderEditButtonClick'
            }
        }, {
            xtype: 'button',
            text: 'Change Order Currency',
            listeners: {
                click: 'onChangeOrderCurrencyButtonClick'
            },
            reference: 'onChangeOrderCurrencyButton'
        }]
    }]
});
