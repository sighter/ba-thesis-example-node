Ext.define('Ord.view.main.ItemForm', {

    extend: 'Ord.base.Form',

    requires: [
        'Ext.form.field.Number',
        'Ext.form.field.Text',
        'Ext.form.field.TextArea',
        'Ord.view.main.CurrencyComboBox',
        'Ord.view.main.ItemFormController'
    ],

    controller: 'itemform',

    alias: 'widget.itemform',

    constructor: function (config) {
        config = config || {};

        config = Ext.apply({}, config, {
            lockCurrency: false
        });

        config.items = [{
            xtype: 'textfield',
            name: 'productSKU',
            fieldLabel: 'Product SKU',
            allowBlank: false
        }, {
            xtype: 'numberfield',
            name: 'price',
            fieldLabel: 'Price',
            allowBlank: false
        }, {
            xtype: 'currencycombobox',
            name: 'priceCurrency',
            fieldLabel: 'Currency',
            allowBlank: false,
            readOnly: config.lockCurrency
        }, {
            xtype: 'numberfield',
            name: 'quantity',
            minValue: 1,
            value: 1,
            fieldLabel: 'Quantity',
            allowBlank: false
        }, {
            xtype: 'textareafield',
            name: 'description',
            fieldLabel: 'Description'
        }];

        this.callParent([config]);
    }
});