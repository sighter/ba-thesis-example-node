Ext.define('Ord.view.main.CurrencySwitcher', {

    extend: 'Ext.form.Panel',

    requires: [
        'Ext.button.Button',
        'Ord.view.main.CurrencyComboBox',
        'Ord.view.main.CurrencySwitcherController'
    ],

    controller: 'currencyswitcher',

    xtype: 'currencyswitcher',

    constructor: function (config) {
        config = config || {};

        config.layout = {
            type: 'hbox',
            align: 'stretch'
        };

        if (config.order) {
            this.order = config.order;
        } else {
            Ext.Error.raise({
                msg: 'An order must be set'
            });
        }

        config.bodyPadding = 10;

        config.items = [{
            xtype: 'currencycombobox',
            reference: 'currencyField',
            margin: '0 5px 0 0'
        }, {
            xtype: 'button',
            text: 'Change Currency',
            listeners: {
                click: 'onChangeOrderCurrencyButtonClick'
            }
        }];

        this.callParent([config]);
    }
});