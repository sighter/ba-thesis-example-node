/**
 * This view is an example list of people.
 */
Ext.define('Ord.view.main.ItemList', {
    extend: 'Ext.grid.Panel',

    xtype: 'itemlist',

    requires: [
        'Ord.store.Item'
    ],

    store: {
        type: 'item'
    },

    emptyText: 'There are no items for selected order',

    columns: [
        { text: 'SKU',  dataIndex: 'productSKU' },
        {
            text: 'Price',
            dataIndex: 'price',
            renderer: function (value) {
                return (value / 100).toFixed(2);
            }
        },
        { text: 'CCY.', dataIndex: 'priceCurrency', width: 60},
        { text: 'QTY.', dataIndex: 'quantity', width: 60},
        { text: 'Created', dataIndex: 'createdAt', flex: 1}
    ],

    listeners: {
        itemdblclick: 'onItemDblClick'
    },

    tbar: [{
        xtype: 'button',
        text: 'Create New Item',
        reference: 'createButton',
        listeners: {
            click: 'onCreateItemButtonClick'
        }
    }]
});
