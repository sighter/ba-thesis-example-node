Ext.define('Ord.view.main.OrderForm', {

    extend: 'Ord.base.Form',

    requires: [
        'Ext.form.field.Text',
        'Ext.form.field.Display',
        'Ext.form.field.TextArea',
        'Ext.form.field.Date',
        'Ord.view.main.OrderFormController'
    ],

    controller: 'orderform',

    alias: 'widget.orderform',

    constructor: function (config) {
        config = config || {};

        config.items = [{
            xtype: 'textfield',
            name: 'orderNr',
            fieldLabel: 'Order-Nr.'
        }, {
            xtype: 'displayfield',
            name: 'price',
            fieldLabel: 'Price',
            renderer: function (value) {
                return (value / 100).toFixed(2);
            },
            hidden: config.mode !== 'display'
        }, {
            xtype: 'displayfield',
            name: 'priceCurrency',
            fieldLabel: 'Currency',
            hidden: config.mode !== 'display'
        }, {
            xtype: 'textareafield',
            name: 'description',
            fieldLabel: 'Description'
        }, {
            xtype: 'datefield',
            name: 'createdAt',
            fieldLabel: 'Created',
            hidden: config.mode !== 'display'
        }];

        this.callParent([config]);
    }
});