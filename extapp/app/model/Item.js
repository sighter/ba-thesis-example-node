Ext.define('Ord.model.Item', {

    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest',
        'Ord.model.Order'
    ],

    entityName: 'Item',

    fields: [
        {name: 'productSKU', type: 'string'},
        {name: 'price', type: 'integer'},
        {name: 'priceCurrency', type: 'string'},
        {name: 'quantity', type: 'integer'},
        {name: 'description', type: 'string'},
        {name: 'createdAt', type: 'date'},
        {name: 'OrderId', reference: 'Order'}
    ],

    proxy: {
        type: 'rest',
        url : '/api/item'
    }
});