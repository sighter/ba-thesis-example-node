Ext.define('Ord.model.Order', {

    extend: 'Ext.data.Model',

    requires: [
        'Ext.data.proxy.Rest'
    ],

    entityName: 'Order',

    fields: [
        {name: 'orderNr', type: 'string'},
        {name: 'price', type: 'integer'},
        {name: 'priceCurrency', type: 'string'},
        {name: 'description', type: 'string'},
        {name: 'createdAt', type: 'date'}
    ],

    proxy: {
        type: 'rest',
        url : '/api/order'
    }
});
