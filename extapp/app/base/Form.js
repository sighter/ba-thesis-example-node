Ext.define('Ord.base.Form', {

    requires: [
        'Ext.data.Model'
    ],

    extend: 'Ext.form.Panel',

    alias: 'widget.recordform',

    constructor: function (config) {
        config = config || {};

        config.bodyPadding = 5;

        config = Ext.apply({}, config, {
            bodyPadding: 5,
            trackResetOnLoad: true,
            mode: 'edit'
        });

        // in create mode, record must be either a string or a record
        if (config.mode === 'create') {
            if (config.record instanceof Ext.data.Model) {
                config.record = config.record;
            } else if (Ext.isString(config.record)) {
                config.record = Ext.create(config.record);
            } else {
                Ext.Error.raise({
                    msg: 'Record must be a string or a Ext.data.Model'
                });
            }
        }

        if (config.record) {
            this.recordToGetLoaded = config.record;
        }

        config.layout = {
            type: 'anchor'
        };

        config.defaults = {
            anchor: '100%',
            readOnly: config.mode === 'display' || false
        };

        if (config.mode === 'edit' || config.mode === 'create') {
            config.bbar = ['->', {
                xtype: 'button',
                hidden: config.mode !== 'create',
                formBind: true,
                reference: 'createButton',
                text: 'Create',
                listeners: {
                    click: this.onCreateButtonClick,
                    scope: this
                }
            }, {
                xtype: 'button',
                hidden: config.mode !== 'edit',
                formBind: true,
                text: 'Save',
                listeners: {
                    click: this.onSaveButtonClick,
                    scope: this
                }
            }, {
                xtype: 'button',
                text: 'Cancel',
                listeners: {
                    click: this.onCancelButtonClick,
                    scope: this
                }
            }];
        }

        this.storeListeners = null;
        this.mode = config.mode;

        this.callParent([config]);
    },

    initComponent: function () {
        this.callParent(arguments);

        if (this.recordToGetLoaded) {
            this.loadRecord(this.recordToGetLoaded);
        }
    },

    loadRecord: function (record) {
        var store, me;

        store = record.store;
        me = this;

        if (this.storeListeners) {
            this.storeListeners.destroy();
        }

        // no bind on edit or create mode
        if (this.mode == 'display') {
            this.storeListeners = store.on({
                destroyable: true,
                update: function (store, record, op) {
                    if (me.getRecord().getId() === record.getId() &&
                        me.mode === 'display' &&
                        Ext.data.Model.EDIT === op
                    ) {
                        me.loadRecord(record);
                    }
                }
            });
        }

        this.callParent(arguments);
    },

    enableCreateButton: function () {
        this.lookupReference('createButton').enable();
    },

    disableCreateButton: function () {
        this.lookupReference('createButton').disable();
    },

    onCancelButtonClick: function (button) {
        if (this.up('window')) {
            this.up('window').close();
        }
    },

    onSaveButtonClick: function (button) {
        var rec = this.getRecord();
        this.updateRecord(rec);

        // if records store has no autosync, call save
        if (rec.store && rec.store.autoSync === false) {
            rec.save();
        }

        if (this.up('window')) {
            this.up('window').close();
        }
    },

    onCreateButtonClick: function (button) {
        var rec = this.getRecord();
        this.updateRecord(rec);
        rec.save();

        if (this.up('window')) {
            this.up('window').close();
        }
    }
});