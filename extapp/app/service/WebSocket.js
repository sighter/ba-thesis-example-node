/**
 * Wrapping service around autobahn, mapping wamp events to WebSocketProxies
 */
Ext.define('Ord.service.WebSocket', {

    requires: [
        'Ext.data.StoreManager',
        'Ext.util.MixedCollection',
        'Ext.util.Observable',
        'Ord.service.Subscription'
    ],

    mixins: {
        observable: 'Ext.util.Observable'
    },
    /**
     * gets fired, then a connecttion to the websocket was established
     *
     * @event connected
     * @param {Object} session an autobahn session
     */

    /**
     * @event hangup
     *
     * event gets fired everytime the WebSockt connection was hung up
     *
     * @param {Utils.websocket.service.WebSocket} this
     */

    /**
     * @event afterdispatch
     *
     * gets fired after an event got dispatched to a proxy and the
     * records are read and edited or inserted
     *
     * @param {Utils.websocket.service.WebSocket} this
     * @param {String} topic
     * @param {Utils.data.proxy.WebSocket} proxy
     * @param {Utils.data.Model} records
     * @param {Object} ret the return object
     * @param {Utils.data.Model[]} ret.updated updated records
     * @param {Utils.data.Model[]} ret.created created records
     * @param {String} action either create, update or null for mixed
     */

    constructor: function (config) {
        this.mixins.observable.constructor.call(this, config);

        this.storeManager = Ext.data.StoreManager;

        /**
         * @property {string}
         *
         * the current state of the webSocketService, can be one of the following
         * strings: "disconnected", "connecting", "connected"
         *
         */
        this.state = "disconnected";

        /**
         * the underlying autobahn session object
         */
        this.session = null;

        this.subs = Ext.create('Ext.util.MixedCollection');

        //this.proxies = Ext.create('Ext.util.MixedCollection');
        this.connect();
    },

    /**
     * dispatch a event to the proxy which has subscribed
     * to the topic
     *
     * @private
     *
     * @param  {String} topic
     * @param  {Utils.data.proxy.WebSocket} proxy
     * @param  {Array} args
     * @param  {Object} kwargs
     * @param  {Object} details
     *
     */
    dispatch: function(topic, sub, args, kwargs, details) {
        // extract data
        var data, action, ret;

        console.debug('dispatching data:', {
            'topic': topic,
            'sub': sub,
            'args': args,
            'kwargs': kwargs,
            'details': details
        });

        if (kwargs.eventType === 'add') {
            this.addDataToStores(args, sub.getStores().items);
        } else if (kwargs.eventType === 'update') {
            this.updateDataInStores(args, sub.getStores().items);
        }

        try {
            data = args[0];
        } catch (e) {
            Ext.Error.raise('Wrong event format. got ', args, kwargs, details);
        }
        //this.fireEvent('afterdispatch', this, topic, proxy, ret, action);
    },


    /**
     * connect to websocket using application config's webSocketHostname
     * field.
     *
     * @private
     */
    connect: function () {
        var hostname, connection, wsUri, me, protocol;

        me = this;
        protocol = 'ws';

        this.state = 'connecting';

        hostname = '127.0.0.1:9000';

        if (!hostname) {
            throw new Error('No webSocketHostname is set.');
        }

        // The protocol at the end is needed for the different apache proxies
        wsUri = protocol + '://' + hostname;

        connection = new autobahn.Connection({
            url: wsUri,
            realm: 'realm1',
            max_retries: 5
        });

        connection.onopen = function (session) {
            // break out of the autobahn scope which shallows errors
            Ext.defer(me.onOpen, 1, me, [session]);
        };

        connection.onclose = function (reason, details) {
            me.onClose(reason, details);
        };

        connection.open();
    },

    /**
     * subscribe a proxy to a wamp topic
     *
     * @private
     */
    subscribeProxy: function (proxy) {
        var session, subscription, topic, me;

        session = this.session;
        topic = proxy.getTopic();
        me = this;

        // be aware, the proxy an the topic is cached via closure
        subscription = session.subscribe(topic, function(args, kwargs, details) {
            me.dispatch(topic, proxy, args, kwargs, details);
        });

        subscription.then(
           function (subscription) {
              me.logger.info('subscription successfull:', subscription, topic);
           },
           function (error) {
              me.logger.error('subscription failed:', subscription, error, topic);
           }
        );
    },

    subscribeStore: function (store) {
        var url, sub, newSub;

        if (store.getProxy() && store.getProxy().getUrl) {
            url = store.getProxy().getUrl();
        } else {
            Ext.Error.raise({
                msg: 'Store needs to have an url'
            });
        }

        sub = this.subs.getByKey(url);


        if (!sub) {
            sub = Ext.create('Ord.service.Subscription', {
                topic: url
            });
            sub.addStore(store);
            this.subs.add(sub);
        } else {
            this.subs.each(function (s) {
                s.removeStore(store);
            });
            sub.addStore(store);
        }

        this.subscribe();
        return sub;
    },

    subscribe: function () {
        var session, me, openSubs;

        me = this;
        session = this.session;

        if (!session) {
            return;
        }

        openSubs = _.filter(this.subs.items, function (s) {
            return s.needsSubscription();
        });

        _.each(openSubs, function (sub) {
            var topic, subscription;

            topic = sub.getTopic();

            if (sub.subscribed === false) {
                // be aware, the proxy an the topic is cached via closure
                subscription = session.subscribe(topic, function(args, kwargs, details) {
                    me.dispatch(topic, sub, args, kwargs, details);
                });

                subscription.then(
                   function (subscription) {
                      console.log('subscription successfull:', subscription, topic);
                      sub.setSubscribed(true);
                   },
                   function (error) {
                      console.error('subscription failed:', subscription, error, topic);
                   }
                );
            }
        });
    },

    /**
     * called everytime a proxy was registered
     *
     * @private
     */
    onProxyAdded: function (proxy) {
        if (this.state === "disconnected") {
            this.connect();
        } else if (this.state === "connected") {
            this.subscribeProxy(proxy);
        }
    },

    /**
     * callback which is called whenever a connection is opened
     *
     * all registered proxies get subscribed, if a connection opens
     *
     * @private
     */
    onOpen: function (session) {
        Ext.log({
            msg: 'Connected to ws',
            session: session
        });
        this.session = session;
        this.state = "connected";
        this.fireEvent('connected', session);

        this.subscribe();
    },

    /**
     * callback which will get called whenever a connection is closed
     *
     * @private
     */
    onClose: function (reason, details) {
        this.state = "disconnected";
        Ext.log({
            msg: 'WebSocket connection closed',
            reason: reason,
            details: details
        });

        if (details && details.retry_count <= 1) {
            // only fire hangup once
            this.fireEvent('hangup', this);
        }
    },

    addDataToStores: function (data, stores) {
        var me;

        me = this;

        _.each(stores, function (store) {
            var reader, resultSet;

            reader = store.getProxy().getReader();
            resultSet = reader.read(data);

            store.suspendAutoSync();
            store.add(resultSet.records);
            store.resumeAutoSync();
        });
    },

    updateDataInStores: function (data, stores) {
        var me;

        me = this;

        _.each(stores, function (store) {
            var reader, resultSet;

            reader = store.getProxy().getReader();
            resultSet = reader.read(data);

            _.each(resultSet.records, function (rec) {
                var candidate = store.getById(rec.getId());

                if (candidate) {
                    store.suspendAutoSync();
                    candidate.set(rec.getData());
                    candidate.commit();
                    store.resumeAutoSync();
                }
            });
        });
    }
});
