
Ext.define('Ord.service.Subscription', {

    requires: [
        'Ext.Error',
        'Ext.mixin.Observable',
        'Ext.util.MixedCollection'
    ],

    mixins: {
        observable: 'Ext.mixin.Observable'
    },

    constructor: function (config) {
        config = config || {};
        this.mixins.observable.constructor.call(this, config);

        if (config.topic) {
            this.topic = config.topic;
            this.id = config.topic;
        } else {
            Ext.Error.raise({
                msg: 'topic is required'
            });
        }

        this.subscribed = false;
        this.subscribing = false;

        this.stores = Ext.create('Ext.util.MixedCollection');

        this.callParent([config]);
    },

    addStore: function (store) {
        this.stores.add(store);
    },

    removeStore: function (store) {
        this.stores.remove(store);
    },

    getStores: function () {
        return this.stores;
    },

    getTopic: function () {
        return this.topic;
    },

    setSubscribed: function () {
        this.subscribed = true;
        this.subscribing = false;
        this.fireEvent('subscribed', this);
    },

    setUnsubscribed: function () {
        this.subscribed = false;
        this.subscribing = false;
    },

    setSubscribing: function () {
        this.subscribing = true;
    },

    needsSubscription: function () {
        return !this.subscribed && !this.subscribing;
    }

});