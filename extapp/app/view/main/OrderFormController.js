Ext.define('Ord.view.main.OrderFormController', {

    extend: 'Ext.app.ViewController',

    alias: 'controller.orderform',

    constructor: function (config) {
        config = config || {};
        this.callParent([config]);
    }
});
