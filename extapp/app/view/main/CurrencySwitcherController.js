Ext.define('Ord.view.main.CurrencySwitcherController', {

    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.Ajax'
    ],

    alias: 'controller.currencyswitcher',

    constructor: function (config) {
        config = config || {};
        this.callParent([config]);
    },

    onChangeOrderCurrencyButtonClick: function () {
        var value;

        value = this.lookupReference('currencyField').getValue();

        if (!value) {
            return;
        }

        Ext.Ajax.request({
            url: '/api/order/' + this.getView().order.getId() + '/changecurrency',
            jsonData: {
                currency: value
            }
        }).then(function(response, opts) {
            var obj = Ext.decode(response.responseText);
            console.dir(obj);

        },
        function(response, opts) {
            console.log('server-side failure with status code ' + response.status);
        });

        this.lookupReference('currencyField').up('window').close();

    }


});
