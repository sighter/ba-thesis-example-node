/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */
Ext.define('Ord.view.main.MainController', {
    extend: 'Ext.app.ViewController',

    requires: [
        'Ext.window.Window',
        'Ord.view.main.OrderForm',
        'Ord.view.main.ItemForm',
        'Ord.view.main.CurrencySwitcher'
    ],

    alias: 'controller.main',

    constructor: function (config) {
        config = config || {};

        this.selectedOrder = null;

        this.callParent([config]);
    },

    onItemSelected: function (sender, record) {
        var form, itemList, items, url, tabPanel, me;

        me = this;

        form = this.lookupReference('orderForm');
        itemList = this.lookupReference('itemList');
        tabPanel = this.lookupReference('tabPanel');

        if (record) {
            tabPanel.enable();
        } else {
            tabPanel.disable();
        }

        this.selectedOrder = record;

        form.loadRecord(record);

        items = record.items();

        this.currentItems = items;

        // todo refactor this with bindings
        url = record.getProxy().getUrl() + '/' + record.getId() + '/items';
        items.getProxy().setUrl(url);

        Ord.app.webSocketService.subscribeStore(items).on({
            subscribed: {
                fn: function (sub) {
                    me.lookupReference('createButton').disable();
                    items.load({
                        scope: me,
                        callback: me.onItemStoreLoaded
                    });
                },
                single: true
            }
        });
        itemList.reconfigure(record.items());
    },

    onItemStoreLoaded: function(records, operation, success) {
        this.lookupReference('createButton').enable();
    },

    onConfirm: function (choice) {
        if (choice === 'yes') {
            //
        }
    },

    onOrderEditButtonClick: function (button) {
        Ext.create('Ext.window.Window', {
            title: 'Edit Order',
            layout: 'fit',
            mode: 'edit',
            width: 400,
            items: [{
                record: this.selectedOrder,
                xtype: 'orderform'
            }]
        }).show();
    },

    onCreateOrderButtonClick:  function (button) {
        Ext.create('Ext.window.Window', {
            title: 'Create Order',
            layout: 'fit',
            width: 400,
            items: [{
                mode: 'create',
                record: 'Ord.model.Order',
                xtype: 'orderform'
            }]
        }).show();
    },

    onCreateItemButtonClick:  function (button) {
        var data, lockCurrency;

        data = {
            quantity: 1
        };

        if (this.currentItems.getCount() > 0) {
            data.priceCurrency = this.currentItems.getAt(0).get('priceCurrency');
            lockCurrency = true;
        }

        Ext.create('Ext.window.Window', {
            title: 'Create Item',
            layout: 'fit',
            width: 400,
            items: [{
                mode: 'create',
                record: Ext.create('Ord.model.Item', data),
                xtype: 'itemform',
                lockCurrency: lockCurrency
            }]
        }).show();
    },

    onItemDblClick: function (grid , record , item , index , e , eOpts ) {
        Ext.create('Ext.window.Window', {
            title: 'Create Item',
            layout: 'fit',
            width: 400,
            items: [{
                mode: 'edit',
                record: record,
                xtype: 'itemform',
                lockCurrency: true
            }]
        }).show();
    },

    onChangeOrderCurrencyButtonClick: function (button) {
        Ext.create('Ext.window.Window', {
            layout: 'fit',
            items: [{
                xtype: 'currencyswitcher',
                order: this.selectedOrder
            }]
        }).show();
    },
});
