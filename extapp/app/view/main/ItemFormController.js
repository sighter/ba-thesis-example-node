Ext.define('Ord.view.main.ItemFormController', {

    extend: 'Ext.app.ViewController',

    alias: 'controller.itemform',

    constructor: function (config) {
        config = config || {};
        this.callParent([config]);
    }
});
