Ext.define('Ord.store.Order', {
    extend: 'Ext.data.Store',

    requires: [
        'Ord.model.Order'
    ],

    autoLoad: true,

    autoSync: true,

    model: 'Ord.model.Order',

    alias: 'store.order',

    storeId: 'order',

    constructor: function (config) {
        config = config || {};

        this.callParent([config]);

        Ord.app.webSocketService.subscribeStore(this);
    }
});
