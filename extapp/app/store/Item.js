Ext.define('Ord.store.Item', {
    extend: 'Ext.data.Store',

    requires: [
        'Ord.model.Item'
    ],

    autoLoad: true,

    autoSync: true,

    model: 'Ord.model.Item',

    alias: 'store.item',

    storeId: 'item',

    constructor: function (config) {
        config = config || {};
        this.callParent([config]);

        Ord.app.webSocketService.subscribeStore(this);
    }
});
