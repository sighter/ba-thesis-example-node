# Example application to show a RESTful integration of WebSockets into rich web applications

## Prerequisites
The application is divided into a frontend application using *ExtJs 6* supported
by *autobahnJS* and *lodash*, and a backend application written in NodeJs using
*express* and *sequelize*. The backend application is also written in ES6 to
support classes and then transpiled via *babel*. To enable communication over
WAMP, the *crossbar* router is used.

To run the application one must install the following dependencies.

Install the crossbar router (as root or with sudo)
```
pip install crossbar
```

In addition to this install NodeJs and NPM

The application is tested with the following versions:

* node 7.2.1
* npm 4.0.5
* crossbar Crossbar.io COMMUNITY 17.3.1 (with python 3.5.2)

## Installing & Running

* clone the repository
* cd into the project dir and run `npm install`
* initialize the test database by running `node src/bin/exampledb.js`
* in a different terminal cd into the project dir and start the crossbar router
via `crossbar start`
* in the first terminal start the app with `npm run build_and_start_production`
* navigate your browser to localhost:3000/app



